window.onbeforeunload = function () {
  const loadingElement = document.createElement('div');
  loadingElement.classList.add('loading');
  document.body.appendChild(loadingElement);
};

window.addEventListener('load', function () {
  const loadingElement = document.querySelector('.loading');
  if (loadingElement) {
    document.body.removeChild(loadingElement);
  }
});

$(document).ready(function () {

  $(".loading").remove();

  if ($(this).scrollTop() > 1) {
    $(".page__header").addClass("fixed");
  } else {
    $(".page__header").removeClass("fixed");
  }
  $(window).scroll(function () {
    if ($(this).scrollTop() > 1) {
      $(".page__header").addClass("fixed");
    } else {
      $(".page__header").removeClass("fixed");
    }
  });

  // Кастомный поиск для Select2
  function matchCustom(params, data) {
    // Если пустой запрос, выводим всё
    if ($.trim(params.term) === '') {
      return data;
    }

    // Скрываем элементы с пустым value
    if (typeof data.element.value === 'undefined') {
      return null;
    }

    // Показываем элементы, где строка входит в value
    if (data.element.value.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
      return data;
    }

    return null;
  }



  // Инициализируем select2 
  if ($("select").length) {
    $("select").each((ind, el) => {
      const select = $(el);
      const isCheker = !select.closest(".popup").length && !select.hasClass("js-delivery-city") && !select.hasClass("js-filter-tags-select-new")

      if (isCheker) {
        // Все селекты не в модалках и не на оформлении заказа
        select.select2({
          minimumResultsForSearch: 5,
          // closeOnSelect: false,
        });
      } else if (select.hasClass("js-delivery-city")) {
        // Селект выбора города для СДЭКа
        select.select2({
          minimumResultsForSearch: 5,
          matcher: matchCustom,
          // closeOnSelect: false,
        });
      } else if (select.hasClass("js-filter-tags-select-new")) {
        select.select2({
          minimumResultsForSearch: Infinity,
          dropdownCssClass: "filter-tags-select__wrap",
        });
      }
    })
  }
  $("a[data-fancybox]").fancybox({
    afterShow: (instance, slide) => {
      // Инициализируем select2 для всех select внутри Fancybox
      setTimeout(function () {
        slide.$content.find("select").select2();
      }, 100);
    },
  });

  // маска для инаупов
  if ($("[data-inputmask]").length) {
    $(":input").inputmask();
  }
  $("input").on("change", function () {
    if ($(this).val().length) {
      $(this).addClass("not-empty");
    } else {
      $(this).removeClass("not-empty");
    }
  });
  $(document).on("change", ".file-input input[type='file']", function () {
    $(this)
      .parents(".file-input")
      .find(".file-input__title")
      .text("Файл выбран");
  });
  // /маска для инпупов
  $(window).resize(function () {
    $(".js-select").select2({
      minimumResultsForSearch: Infinity,
    });
  });
  //  меню услуг
  $(".js-city .city__active").on("click", function () {
    var $cityBlock = $(this).closest(".js-city");
  
    $(this).toggleClass("open");
    $cityBlock.find(".js-city-list").toggle(); 
    $cityBlock.find(".city__ul li:nth-child(2)").addClass("active");

    $(".js-catalog-menu").removeClass("open");
    $("body").removeClass("fixed");
    $(".js-catalog-menu-list").slideUp();
});

$(".js-city-search").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    var $cityBlock = $(this).closest(".js-city");
    $cityBlock.find("a.city").filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
    });
});


  $(".js-adaptive-menu-opener").on("click", function () {
    $(this).parents(".js-adaptive-menu").toggleClass("open");
    $(".js-adaptive-menu-inside").slideToggle();
  });

  $(".js-catalog-menu-opener").on("click", function () {
    $(this).parents(".js-catalog-menu").toggleClass("open");
    $("body").toggleClass("fixed");
    $(".js-catalog-menu-list").slideToggle();
  });

  $(".js-adaptive-menu-catalog-opener").on("click", function () {
    $(this).toggleClass("open");
    $(".js-adaptive-catalog-menu-list").slideToggle();
  });

  $(".js-header-search-input").on("focus", function () {
    $(".js-catalog-menu").removeClass("open");
    $("body").removeClass("fixed");
    $(".js-catalog-menu-list").slideUp();
  });

  $(".js-header-search-input").on("input", function () {
    let val = $(this).val();
    if (val.length) {
      $(".js-header-search-results").show();
    } else {
      $(".js-header-search-results").hide();
    }
  });
  $(".header-search__cross").on("click", function () {
    $(".js-header-search-input").val("");
    $(".js-header-search-results").hide();
  });

  $(".js-footer-scrolltop").on("click", function () {
    $("html, body").animate({ scrollTop: 0 }, 0);
  });


  $(".js-footer-title").on("click", function () {
    $(this).toggleClass("open");
    $(this)
      .parents(".js-footer-block")
      .find(".js-footer-block-content")
      .slideToggle();
  });

  $(".js-faq-question").on("click", function () {
    if (!$(this).hasClass("open")) {
      $(".js-faq-question.open")
        .parents(".js-faq-item")
        .find(".js-faq-answer")
        .slideUp();
      $(".js-faq-question.open").removeClass("open");
    }
    $(this).toggleClass("open");
    $(this).parents(".js-faq-item").find(".js-faq-answer").slideToggle();
  });

  $(".js-filter-title").on("click", function () {
    $(this).toggleClass("open");
    $(this)
      .parents(".js-filter-block")
      .find(".js-filter-content")
      .slideToggle();
  });

  $(".catalog-menu__search input").on("input", function (e) {
    const searchValue = $(this).val().toLowerCase();
    const catalogMenuBlock = $(this)
      .parents(".catalog-menu")
      .find(".catalog-menu__block");
    catalogMenuBlock.find(".catalog-menu__item").each(function () {
      const listItemText = $(this).find("span").text().toLowerCase();
      if (listItemText.startsWith(searchValue)) {
        $(this).show();
        $(this).removeClass("hide-block");
      } else {
        $(this).hide();
        $(this).addClass("hide-block");
      }
    });
    catalogMenuBlock.each(function () {
      if (
        $(this).find(".catalog-menu__item").length ==
        $(this).find(".hide-block").length
      ) {
        $(this).hide();
      } else {
        $(this).show();
      }
    });
  });

  $(".adaptive-menu__catalog-search input").on("input", function (e) {
    const searchValue = $(this).val().toLowerCase();
    const catalogMenuBlock = $(this)
      .parents(".adaptive-menu__catalog-list")
      .find(".adaptive-menu__catalog-block");
    catalogMenuBlock.find(".adaptive-menu__catalog-item").each(function () {
      const listItemText = $(this).find("span").text().toLowerCase();
      if (listItemText.startsWith(searchValue)) {
        $(this).show();
        $(this).removeClass("hide-block");
      } else {
        $(this).hide();
        $(this).addClass("hide-block");
      }
    });
    catalogMenuBlock.each(function () {
      if (
        $(this).find(".adaptive-menu__catalog-item").length ==
        $(this).find(".hide-block").length
      ) {
        $(this).hide();
      } else {
        $(this).show();
      }
    });
  });

  if ($(".js-main-banner").length) {
    var swiper = new Swiper(".js-main-banner", {
      loop: true,
      pagination: {
        el: ".js-main-banner-pagination",
      },
      navigation: {
        nextEl: ".js-main-banner-next",
        prevEl: ".js-main-banner-prev",
      },
    });
  }
  console.log("here")
  if ($(".js-filter-tabs").length) {
    console.log(".js-filter-tabs")
    var swiper = new Swiper(".js-filter-tabs", {
      slidesPerView: "auto",
      spaceBetween: 10,
      scrollbar: {
        el: ".js-swiper-scrollbar-filter-tabs"
      },
    });
  }

  if ($(".js-filter-vals").length) {
    var swiper = new Swiper(".js-filter-vals", {
      slidesPerView: "auto",
      spaceBetween: 10,
    });
  }

  $(".js-tab").on("click", function (e) {
    e.preventDefault();
    let href = $(this).attr("href");
    if (!$(this).hasClass("active")) {
      $(".js-tab.active").removeClass("active");
      $(".js-tab-content.active").removeClass("active").hide();
    }
    $(this).toggleClass("active");
    $(href).toggleClass("active").toggle();

    // if ($(".js-services").length) {
    //   if ($(".js-services-items").outerHeight() > 350) {
    //     $(".js-services").addClass("has-more");
    //   }
    // }
    if ($(window).width() < 600) {
      if ($(".js-services-items").length) {
        var swiper = new Swiper(".services__inside", {
          wrapperClass: "js-services-items",
          slideClass: "services__item",
          slidesPerView: "auto",
          spaceBetween: 10,
        });
      }
    }
  });

  if ($(".js-about-swiper").length) {
    var swiper = new Swiper(".js-about-thumbs", {
      loop: true,
      spaceBetween: 10,
      slidesPerView: 8,
      freeMode: true,
      watchSlidesProgress: true,
      breakpoints: {
        0: {
          slidesPerView: 4,
          spaceBetween: 10,
        },
        1400: {
          slidesPerView: 6,
          spaceBetween: 10,
        },
        1600: {
          slidesPerView: 8,
        },
      },
    });
    var swiper2 = new Swiper(".js-about-swiper", {
      loop: true,
      spaceBetween: 10,
      navigation: {
        nextEl: ".js-about-next",
        prevEl: ".js-about-prev",
      },
      thumbs: {
        swiper: swiper,
      },
      breakpoints: {
        0: {
          slidesPerView: 1,
          spaceBetween: 10,
        },
        400: {
          slidesPerView: 2,
          spaceBetween: 10,
        },
        700: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        1200: {
          slidesPerView: 1,
          spaceBetween: 10,
        },
      },
    });
  }

  $(".js-video-play").on("click", function () {
    console.log("play");
    var video = $(this).parents(".js-video").find("video").get(0);

    if (video.paused) {
      video.play();
      video.setAttribute('controls', 'controls');
      $(this).parents(".js-video").addClass("play");
    } else {
      video.pause();
      $(this).parents(".js-video").removeClass("play");
      video.removeAttribute('controls');
    }
  });

  if ($(".js-video-video").length) {
    document.body.click();
    let countvideo = 0;
    $(window).scroll(function () {
      if (countvideo == 0) {
        countvideo++;
        var video = $(".js-video-video").get(0);
        video.play();
        video.removeAttribute('controls');
      }
    })

    if (countvideo == 0) {
      $('body').on('click', function () {
        countvideo++;
        var video = $(".js-video-video").get(0);
        video.play();
        video.removeAttribute('controls');
      })
    }

  }

  if ($(".js-blog-swiper").length) {
    var swiper = new Swiper(".js-blog-swiper", {
      loop: true,
      spaceBetween: 10,
      navigation: {
        nextEl: ".js-blog-swiper-next",
        prevEl: ".js-blog-swiper-prev",
      },
      breakpoints: {
        0: {
          slidesPerView: "auto",
        },
        340: {
          slidesPerView: "auto",
        },
        1030: {
          slidesPerView: 3,
          spaceBetween: 20,
        },
      },
    });
  }

  if ($(".js-servises-swiper").length) {
    var swiper = new Swiper(".js-servises-swiper", {
      loop: true,
      spaceBetween: 10,
      navigation: {
        nextEl: ".js-servises-swiper-next",
        prevEl: ".js-servises-swiper-prev",
      },
      breakpoints: {
        0: {
          slidesPerView: "auto",
        },
        340: {
          slidesPerView: "auto",
        },
        1030: {
          slidesPerView: 3,
        },
        1200: {
          slidesPerView: 4,
        },
      },
    });
  }

  if ($(".js-reviews").length) {
    var swiper = new Swiper(".js-reviews", {
      loop: true,
      spaceBetween: 20,
      // speed: 5000,
      // autoplay: {
      //   delay: 0,
      //   pauseOnMouseEnter: true,
      // },
      navigation: {
        nextEl: ".js-reviews-next",
        prevEl: ".js-reviews-prev",
      },
      breakpoints: {
        0: {
          slidesPerView: "auto",
          spaceBetween: 10,
        },
        340: {
          slidesPerView: "auto",
          spaceBetween: 10,
        },
        1030: {
          slidesPerView: 3,
          spaceBetween: 40,
        },
        1200: {
          slidesPerView: 4,
          spaceBetween: 40,
        },
      },
    });
  }

  if ($(".js-product-detail").length) {
    var swiper = new Swiper(".js-product-detail", {
      pagination: {
        el: ".js-product-detail-pagination",
      },
      navigation: {
        nextEl: ".js-product-detail-next",
        prevEl: ".js-product-detail-prev",
      },
    });
  }
  if ($(".js-services").length) {
    if ($(".js-services-items").outerHeight() > 350) {
      $(".js-services").addClass("has-more");
    }
  }

  $(".js-services-show").on("click", function () {
    $(".js-services").removeClass("has-more");
    $(this).hide();
    $(".js-services-hide").show();
  });
  $(".js-services-hide").on("click", function () {
    $(".js-services").addClass("has-more");
    $(this).hide();
    $(".js-services-show").show();
  });
  if ($(".js-product-related-slider").length) {
    var swiper = new Swiper(".js-product-related-slider", {
      slidesPerView: "auto",
      spaceBetween: 10,
      pagination: {
        el: ".js-product-related-slider-pagination",
        type: "progressbar",
      },
    })
  }
  if ($(".js-cart-related-slider").length) {
    var swiper = new Swiper(".js-cart-related-slider", {
      slidesPerView: "auto",
      spaceBetween: 10,
      pagination: {
        el: ".js-cart-related-slider-pagination",
        type: "progressbar",
      },
      navigation: {
        nextEl: ".js-cart-related__next",
        prevEl: ".js-cart-related__prev",
      },
    })
  }

  var swiper = new Swiper(".js-product-slider", {
    pagination: {
      el: ".swiper-pagination",
      type: "progressbar",
    },
    navigation: {
      nextEl: ".js-product-slider-next",
      prevEl: ".js-product-slider-prev",
    },
    breakpoints: {
      0: {
        slidesPerView: "auto",
        spaceBetween: 10,
      },
      340: {
        slidesPerView: "auto",
        spaceBetween: 10,
      },
      600: {
        slidesPerView: 2,
        spaceBetween: 10,
      },
      1030: {
        slidesPerView: 3,
        spaceBetween: 20,
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 20,
      },
      1500: {
        slidesPerView: 5,
        spaceBetween: 20,
      },
    },
  });

  if ($(".js-review").length) {
    let i = 0;
    $(".js-review").each(function () {
      i++;
      console.log($(this));
      $(this)
        .parent()
        .find(".js-review-prev")
        .addClass("prev" + i);
      $(this)
        .parent()
        .find(".js-review-next")
        .addClass("next" + i);
      var swiper = new Swiper(this, {
        loop: true,
        spaceBetween: 10,
        navigation: {
          nextEl: ".js-review-prev.prev" + i,
          prevEl: ".js-review-next.next" + i,
        },
      });

      // var swiper = new Swiper($(this), {
      //   loop: true,
      //   spaceBetween: 10,
      //   navigation: {
      //     nextEl: $(this).find(".js-review-prev"),
      //     prevEl: $(this).find(".js-review-next"),
      //   },
      // });
    });
  }

  $(".js-cart-delivery-tab a").on("click", function (e) {
    e.preventDefault();
    let href = $(this).attr("href");
    $(".js-cart-delivery-tab.active").removeClass("active");
    $(".js-cart-delivery-tab-content.active").removeClass("active");
    $(this).parents(".js-cart-delivery-tab").addClass("active");
    $(href).addClass("active");
  });

  $(".js-points__item").on("click", function (e) {
    $(".js-points__item.active").removeClass("active");
    $(this).addClass("active");
  });

  $(".js-points__tab").on("click", function (e) {
    e.preventDefault();
    let href = $(this).attr("href");
    $(".js-points__tab.active").removeClass("active");
    $(".js-points__content.active").removeClass("active");
    $(this).addClass("active");
    $(href).addClass("active");
  });

  $(".counter__add").on("click", function (e) {
    e.preventDefault();
    let val = parseInt(
      $(this).parents(".counter").find(".counter__field").val()
    );
    $(this).parents(".counter").find(".counter__field").val(++val);
  });
  $(".counter__remove").on("click", function (e) {
    e.preventDefault();
    let val = parseInt(
      $(this).parents(".counter").find(".counter__field").val()
    );
    if (val == 1) {
      return;
    }
    $(this).parents(".counter").find(".counter__field").val(--val);
  });
  $(".counter__field").on("keydown", function (e) {
    if (
      $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
      (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
      (e.keyCode >= 35 && e.keyCode <= 40)
    ) {
      return;
    }
    if (
      e.shiftKey ||
      ((e.keyCode < 48 || e.keyCode > 57) &&
        (e.keyCode < 96 || e.keyCode > 105))
    ) {
      e.preventDefault();
    }
  });

  $(".js-filter-tabs-tab").on("click", function (e) {
    e.preventDefault();
    // if (!$(this).hasClass("active")) {
    //   let hrefActive = $(".js-filter-tabs-tab.active").attr("href");
    //   $(".js-filter-tabs-tab.active").removeClass("active");
    //   $(hrefActive).removeClass("active").slideUp();
    // }
    // let href = $(this).attr("href");
    // $(this).toggleClass("active");
    // $(href).toggleClass("active").slideToggle();
  });
  // $(".filter-tabs__val").on("click", function (e) {
  //   // e.preventDefault();
  //   $(".js-filter-tabs-tab:not(.active)")
  //     .find(".filter-tabs__val.active")
  //     .removeClass("active");
  //   let href = $(this).attr("href");
  //   $(this).toggleClass("active");
  //   $(href).toggleClass("active").slideToggle();
  // });

  $(".product__info").on("click", function (e) {
    $(this).parents(".product__infoblock").find(".product__infoplash").toggle();
  });
  $(document).click(function (event) {
    if (!$(event.target).closest(".product__infoblock").length) {
      // Клик произошел вне блока '.product__infoblock'
      $(".product__infoplash").hide(); // Скрываем дополнительный блок
    }
  });
  $(".product__infoblock").each(function () {
    let left = $(this).offset().left;
    var parentWidth = $(this).parents(".product__content").width();
    var parentLeft = $(this).parents(".product__content").offset().left;
    var parentRight = parentLeft + parentWidth;
    console.log(left, parentLeft, parentRight);

    if (left + 200 > parentRight && left > 200) {
      $(this).addClass("right");
    } else {
      $(this).removeClass("right");
    }
    if (left + 200 > parentRight && left < 200) {
      $(this).addClass("midle");
    } else {
      $(this).removeClass("midle");
    }
  });

  $(".js-mobile-search").on("click", function (e) {
    $(".header__center").addClass("active");
  });
  $(".js-mobile-search-hide").on("click", function (e) {
    $(".header__center").removeClass("active");
  });


  $(".region > span").on("click", function () {
    $(".region.active").removeClass("active");
    $(this).parents(".region").addClass("active");
  });
  $(".js-rerion-search").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    $(".city__ul .region>span").filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
    });
  });
  

  $(".js-show-all").on("click", function (e) {
    e.preventDefault();
    $(".marks__item").show();
    $(this).hide();
  });

  window.getTimeline = function ($target, options = {}) {

    let top = $(this).scrollTop();
    let bottom = top + $(window).height();

    if ($target.hasClass("init") || $target.offset().top > bottom + 50) {
      return;
    }

    $target.addClass("init");
    const $elements = $target.find("[data-animate]");

    const timeline = anime.timeline();
    const reverse = options.reverse || false;
    const isOut = options.isOut || false;

    $elements.each((i, element) => {
      const $element = $(element);

      const data = $element.data("animate");

      const keyframes = (isOut ? data.keyframesOut : data.keyframesIn) || {};
      const from = (reverse ? keyframes.to : keyframes.from) || {
        opacity: 0,
      };
      const to = (reverse ? keyframes.from : keyframes.to) || {
        opacity: 1,
      };
      const duration = data.duration || options.duration || 600;
      const easing = data.easing || options.easing || "easeOutCubic";
      const offset = data.offset || options.offset || 400;

      const normalizeKeyframes = $.extend(
        {},
        ...getKeyframes(from, to, duration)
      );

      if ($element.offset().top < $(this).scrollTop()) {
        setTimeout(() => {
          $element.css('opacity', 1)
          $element.css('transform', 'translateY(0px)')
        }, 500);

      } else {

        timeline.add(
          $.extend({}, normalizeKeyframes, {
            targets: element,
            easing,
            duration,
            autoplay: false,
            offset: i === 0 ? 0 : `-=${offset}`,
          })
        );
      }

    });

    return timeline;
  }

  window.getKeyframes = function (from, to, duration) {
    const keys = Object.keys(from);

    return keys.map((key) => {
      return {
        [key]: [
          { value: from[key], duration: 0 },
          { value: to[key], duration },
        ],
      };
    });
  }

  const $dataElements = $("[data-elements]");
  $dataElements.each(function () {
    const $this = $(this);
    getTimeline($this);
  });

  $(window).scroll(function () {
    $dataElements.each(function () {
      const $this = $(this);
      getTimeline($this);
    });
  });



  if ($('#map').length) {
    const coords = $('#map').data('coords').split(",").map(Number);
    const zoom = 15
    ymaps.ready(function () {
      var myMap = new ymaps.Map(
        "map",
        {
          center: coords,
          zoom: 15,
          controls: [],
        },
        {}
      );
  
      var myPlacemark = new ymaps.Placemark(
        coords,
        {},
        {
          iconLayout: "default#image",
          iconImageHref: "https://porogich.ru/local/templates/porog/img/icons/map-new.svg",
          iconImageSize: [100, 107],
          iconImageOffset: [-50, -53],
        }
      );
  
      myMap.geoObjects.add(myPlacemark);
  
      setMapCenter();

      $(window).on('resize', function () {
        setMapCenter();
      });
  
      function setMapCenter() {
        if ($(window).width() < 600) {
          myMap.setCenter(coords);
        } else {
          
          myMap.setCenter([coords[0], coords[1] - 0.01]);
        }
      }
    });
  }
  
  


  $(".js-show-partners-title").on("click", function (e) {
    $(".js-show-partners").toggleClass("active");
    $(".js-partners-content").slideToggle();
  });


  console.log($(window).width())
  if ($(window).width() < 600) {

    if ($(".js-services-items").length) {
      console.log('here3')
      var swiper = new Swiper(".services__inside", {
        wrapperClass: "js-services-items",
        slideClass: "services__item",
        slidesPerView: "auto",
        spaceBetween: 10,
      });
    }
  }

  if ($(".js-swiper-filters").length) {
    var swiper = new Swiper(".js-swiper-filters", {
      slidesPerView: "auto",
      spaceBetween: 10,
      navigation: {
        nextEl: ".js-swiper-filters__next",
        prevEl: ".js-swiper-filters__prev",
      },
      scrollbar: {
        el: ".js-swiper-filters__scrollbar",
      },
    });
  }

  $('.js-all-checkbox input').on('change', function(e) { 
    $('.js-cart-item-checkbox input').prop('checked', $(this).prop('checked'));
});

$('.js-cart-tabs input').on('change', function(e) { 
  if($(this).val() == 'client') {
    $('.js-tab-info').show()
  } else {
    $('.js-tab-info').hide() 
  }
});

});
